
import os
from flask import Flask, redirect, url_for, render_template, request, send_from_directory,flash
from flask_login import LoginManager, UserMixin,  current_user, login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from pony.orm import Database, Required, Optional, db_session, select,PrimaryKey,Set, commit, count, desc
import keygen
from forms import LoginForm
from werkzeug.utils import secure_filename


UPLOAD_FOLDER = '/home/dafinaMarku/finalprojecticts/images'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.secret_key = keygen.generate()
login = LoginManager(app)
login.login_view = 'login'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

db = Database()

class User(UserMixin, db.Entity):

    id = PrimaryKey(int, auto=True)
    username = Required(str, unique=True)
    password_hash = Optional(str)
    followers=Set('User')
    following=Set('User')
    artworks=Set('Artwork')
    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

class Artwork(db.Entity):
    id = PrimaryKey(int, auto=True)
    title=Required(str, unique=True)
    description=Optional(str)
    author=Required(User)
    category=Required('Category')
    styles=Set('Style')
    subject=Required('Subject')
    image=Optional('ArtworkImage')

class ArtworkImage(db.Entity):
    filename=Required(str)
    artwork=Required('Artwork')

class Category(db.Entity):
    id = PrimaryKey(int, auto=True)
    name=Required(str, unique=True)
    artworks=Set('Artwork')

class Style(db.Entity):
    id = PrimaryKey(int, auto=True)
    name=Required(str,unique=True)
    artworks=Set('Artwork')

class Subject(db.Entity):
    id = PrimaryKey(int, auto=True)
    name=Required(str,unique=True)
    artworks=Set('Artwork')


db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)

@db_session
def createEntities():

    Category(name='Painting')
    Category(name='Drawing')
    Style(name='Realism')
    Style(name='Hyper Realism')
    Style(name='Pop Art')
    Style(name='Abstract')
    Subject(name='Nature')
    Subject(name='Portrait')
    Subject(name='Landscape')

#createEntities()


@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))

@app.route('/')
@db_session
def firstPage():
    if request.method=='GET':
        searchtext=request.args.get('searchartwork','')
        if searchtext=='':
            result=None
        else:
            result=select(a for a in Artwork if searchtext.upper() in a.title.upper())
        categories=select(c.name for c in Category)
        styles=select(p.name for p in Style)
        subjects=select(p.name for p in Subject)
        if current_user.is_authenticated:
            return render_template('homepage.html', CATEGORIES=categories, STYLES=styles, SUBJECTS=subjects, NAME=current_user.username, SEARCHRESULTS=result)
        return render_template('homepage.html', CATEGORIES=categories, STYLES=styles, SUBJECTS=subjects, SEARCHRESULTS=result)

@app.route('/explore/<type>/<value>')
@db_session
def explore(type, value):
    if request.method=='GET':
        if type:
            if value:
                if type=='subject':
                    result=select(a for a in Artwork if a.subject.name==value)
                elif type=='style':
                    searchStyle=Style.get(name=value)
                    result=select(a for a in Artwork if searchStyle in a.styles)
                else:
                     return render_template('404.html'), 404
                categories=select(c.name for c in Category)
                if(current_user.is_authenticated):
                    return render_template('explore.html', NAME=current_user.username, CATEGORIES=categories, RESULTS=result)
                return render_template('explore.html', CATEGORIES=categories, RESULTS=result)
        return render_template('404.html'), 404

@app.route('/profile')
@db_session
@login_required
def index():
    nrfollowers=count(u.followers for u in User if u.id==current_user.id)
    nrfollowing=count(u.following for u in User if u.id==current_user.id)
    categories=select(p.name for p in Category)
    artworks=select(a for a in Artwork if a.author==current_user).order_by(desc(Artwork.id))
    return render_template('index.html', CATEGORIES=categories, NAME=current_user.username,ARTWORKS=artworks, NRFOLLOWERS=nrfollowers, NRFOLLOWING=nrfollowing)

@app.route('/ourartists')
@db_session
def ourartists():
    categories=select(c.name for c in Category)
    if current_user.is_authenticated:
        artists=select(a for a in User if a.id != current_user.id)
        return render_template('ourartists.html', CATEGORIES=categories, ARTISTS=artists, NAME=current_user.username)
    else:
        artists=select(a for a in User)
        return render_template('ourartists.html', CATEGORIES=categories, ARTISTS=artists )

@app.route('/viewArtwork/<id>', methods=['GET', 'POST'])
@db_session
def viewArtwork(id):
    a=select(a for a in Artwork if a.id==id)
    if a:
       categories=select(p.name for p in Category)
       if current_user.is_authenticated:
            return render_template('viewArtwork.html',CATEGORIES=categories, NAME=current_user.username, ARTWORK= Artwork[id] )
       return render_template('viewArtwork.html',CATEGORIES=categories, ARTWORK= Artwork[id] )
    return render_template('404.html'), 404

@app.route('/deleteArtwork/<id>', methods=['GET', 'POST'])
@login_required
@db_session
def deleteArtwork(id):
    a=select(a for a in Artwork if a.id==id)
    if a:
        ArtworkImage.get(artwork=Artwork[id]).delete()
        Artwork[id].delete()

        return redirect(url_for('index'))
    return render_template('404.html'), 404

@app.route('/editArtwork/<id>', methods=['GET', 'POST'])
@login_required
@db_session
def editArtwork(id):
    categories=select(p.name for p in Category)
    styles1=select(p.name for p in Style)
    subjects=select(p.name for p in Subject)
    if request.method=='GET':
        a=select(a for a in Artwork if a.id==id)
        if a:
           return render_template('editArtwork.html',CATEGORIES=categories, STYLES=styles1, SUBJECTS=subjects, NAME=current_user.username, ARTWORK= Artwork[id])
        return render_template('404.html'), 404
    #request method is post
    t=request.form.get('title')
    s=request.form.getlist('styles')
    c=request.form['category']
    sub=request.form['subject']
    desc=request.form.get('description')
    imagename=request.form.get('imagename')
    styles=[]
    for style in s:
        styles.append(Style.get(name=style))
    if not s:
        error="You should pick at least one style."
        return render_template('editArtwork.html', CATEGORIES=categories, SUBJECTS=subjects, STYLES=styles1, NAME=current_user.username, ERRORSTYLE=error)
    elif t and c and sub:
        titleexists=select(a.title for a in Artwork if a.title==t and a != Artwork[id])
        if titleexists:
            error="The title you choose was taken"
            return render_template('editArtwork.html', CATEGORIES=categories, SUBJECTS=subjects, ARTWORK=Artwork.get(title=t), STYLES=styles1,NAME=current_user.username, ERROR=error)
        file=request.files['file']
        if file and allowed_file(file.filename): #image change
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            Artwork[id].set(title=t, category=Category.get(name=c), subject=Subject.get(name=sub), styles=styles)
            a=Artwork[id]
            ArtworkImage.get(artwork=a).set(filename=imagename, artwork=a)
        else:
            a=Artwork[id].set(title=t, category=Category.get(name=c), subject=Subject.get(name=sub), styles=styles, description=desc)
    return redirect(url_for('index'))


@app.route('/artistSearch', methods=['GET', 'POST'])
@login_required
@db_session
def artistSearch():
    categories=select(p.name for p in Category)
    if request.method=="GET":
        searchText=request.args.get('searchArtist')
        result=select(u for u in User if searchText.upper() in u.username.upper() and u.username != current_user.username)
        return render_template('ArtistSearchResult.html', CATEGORIES=categories,  NAME=current_user.username, ARTISTS=result)
    return redirect(url_for('index'))

@app.route('/VisitProfile/<id>', methods=['GET', 'POST'])
@db_session
def VisitProfile(id):
    #will verify if current_user is following or not the visited profile
    query=select(u for u in User if current_user in u.followers and u.id==id)
    if request.method=="GET":
        artist=select(a for a in User if a.id==id)
        categories=select(p.name for p in Category)
        if artist:
            art=select(a for a in Artwork if a.author==User[id]).order_by(desc(Artwork.id))
            if current_user.is_authenticated:
                #current_user can not visit his own profile the same way he visits other's profile
                if id != current_user.id:
                    nrfollowers=count(u.followers for u in User if u.id==id)
                    nrfollowing=count(u.following for u in User if u.id==id)
                    if query:
                        followsOrNot="Unfollow"
                    else:
                        followsOrNot="Follow"
                    return render_template('VisitProfile.html',ARTIST=User[id],NRFOLLOWERS=nrfollowers,NRFOLLOWING=nrfollowing,FOLLOWRESULT=followsOrNot,ARTWORKS=art,CATEGORIES=categories,NAME=current_user.username)
                else:
                    return render_template('404.html'), 404
            else:
                return render_template('VisitProfile.html',ARTIST=User[id],ARTWORKS=art,CATEGORIES=categories)
        return render_template('404.html'), 404
    #method is post user clicks at Follow/Unfolow button
    if query:#user is following the artist so when the button is clicked he/she unfollows the artist
        User[id].followers.remove(User[current_user.id])
    else:
        User[id].followers.add(User[current_user.id])
    return redirect(url_for('VisitProfile',id=id))


@app.route('/<choosedcategory>')
@db_session
def PaintingsDrawings(choosedcategory):
    if choosedcategory:
        result=select(a for a in Artwork if a.category.name==choosedcategory)
        if result:
            categories=select(c.name for c in Category)
            if current_user.is_authenticated:
                return render_template('PaintingsDrawings.html', ARTWORKS=result, NAME=current_user.username, CATEGORIES=categories)
            return render_template('PaintingsDrawings.html', ARTWORKS=result, CATEGORIES=categories)
    return render_template('404.html'), 404



@app.route('/FollowersFollowing/<req>/<int:id>', methods=['GET', 'POST'])
@login_required
@db_session
def FollowersFollowing(req, id):
    categories=select(p.name for p in Category)
    if request.method=='GET':
        u=select(u for u in User if u.id==id)
        if u:
            result=[]
            if req=='followers':
                searchtext=request.args.get('searchfollowers', '')
                userfollowers=User[id].followers
                for u in userfollowers:
                    if searchtext.upper() in u.username.upper():
                        result.append(u)
                #PAGEOF represent the user to whom the search page belongs to
                return render_template('FollowersFollowing.html', FOLLOWING=None, FOLLOWERS=result,PAGEOF=User[id], NAME=current_user.username, CATEGORIES=categories)
            elif req=='following':
                searchtext=request.args.get('searchfollowing', '')
                userfollowing=User[id].following
                for u in userfollowing:
                    if searchtext.upper() in u.username.upper():
                        result.append(u)
                return render_template('FollowersFollowing.html', FOLLOWERS=None, FOLLOWING=result,PAGEOF=User[id], NAME=current_user.username, CATEGORIES=categories)
        return render_template('404.html'), 404


@app.route('/unfollow/<req>/<int:id>', methods=['GET', 'POST'])
@login_required
@db_session
def unfollow(req, id):
    query=select(u for u in User if current_user in u.followers and u.id==id)
    if query:
        User[id].followers.remove(User[current_user.id])
        return redirect(url_for('FollowersFollowing', req=req,id=current_user.id))
    return render_template('404.html'), 404

#timeline of current_user
@app.route('/timeline')
@login_required
@db_session
def timeline():
    categories=select(p.name for p in Category)
    followingart=select(a for a in Artwork if current_user in a.author.followers).order_by(desc(Artwork.id))
    return render_template('timeline.html', FOLLOWINGART=followingart, NAME=current_user.username, CATEGORIES=categories)

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('firstPage'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))

        login_user(user)  # remember=form.remember_me.data)
        return redirect(url_for('firstPage'))

    return render_template('login.html', title='Sign In', form=form)

@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':
        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'])
        u.set_password(data['password'])

        return redirect(url_for('login'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')

@db_session
@login_required
@app.route('/select')
def selectTest():
    return render_template('selectTest.html')

@app.route('/addArtwork', methods=['GET', 'POST'])
@login_required
@db_session
def addArtwork():
    categories=select(p.name for p in Category)
    styles1=select(p.name for p in Style)
    subjects=select(p.name for p in Subject)
    if request.method=="GET":
        return render_template('NewArtwork.html', CATEGORIES=categories, SUBJECTS=subjects, STYLES=styles1,NAME=current_user.username)
    elif request.method=='POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            t=request.form.get('title')
            s=request.form.getlist('styles')
            c=request.form['category']
            sub=request.form['subject']
            desc=request.form.get('description')
            imagename=request.form.get('imagename')
            author=User[current_user.id]
            styles=[]
            for style in s:
                styles.append(Style.get(name=style))
            if not s:
                error="You should pick at least one style."
                return render_template('NewArtwork.html', CATEGORIES=categories, SUBJECTS=subjects, STYLES=styles1,NAME=current_user.username, ERRORSTYLE=error)
            elif t and c and sub and imagename:
                titleexists=select(a.title for a in Artwork if a.title==t)
                if titleexists:
                    error="The title you choose was taken"
                    return render_template('NewArtwork.html', CATEGORIES=categories, SUBJECTS=subjects, STYLES=styles1,NAME=current_user.username, ERROR=error)
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                a=Artwork(title=t, category=Category.get(name=c), styles=styles, subject=Subject.get(name=sub), description=desc, author=author)
                ArtworkImage(filename=imagename, artwork=a)
                return redirect(url_for('index'))
            else:
                return redirect(url_for('addArtwork'))


if __name__ == '__main__':
    app.run(threaded=True, port=5000)

